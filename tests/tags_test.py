import unittest
from src.tags_search import *

class MyTestCase(unittest.TestCase):
    def test_multiple_tags(self):
        tags = [['Klapki', 'Brak/Płaski', 'Wsuwane', 'Średni', '<none>', 'Letnie', '<none>',
                      'Czarny', 'Skóra/Ekoskóra/ImitajcaSkóry', 'Neutralntesty', '<none>',
                      'Inny materiał']]
        desc = "Czarne czółenka czarnym na słupku, wkładka ze czółenka na średnim obcasie niewielka, wsuwane, z ekoskóry."
        no_of_tags = tags_search(tags,desc)
        self.assertEqual(no_of_tags, 3)

    def test_separated_tags(self):
        pass

    def test_double_word_tags(self):
        pass

    def test_original_vs_lemma(self):
        pass


if __name__ == '__main__':
    unittest.main()
