import tensorflow as tf


class TagsEncoder(tf.keras.Model):

    def __init__(self, embedding_dim, encoding='ohe', vocab_size=0, embeddings=None,emb_divider=10):
        super(TagsEncoder, self).__init__()
        self.encoding = encoding
        if self.encoding == 'ohe':
            self.fc = tf.keras.layers.Dense(embedding_dim)

        if self.encoding == 'emb':
            self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim, weights=[embeddings],
                                                        trainable=False)
            self.flat = tf.keras.layers.Flatten()
            self.emb_divider = emb_divider

    def call(self, x):
        if self.encoding == 'ohe':
            x = self.fc(x)
            x = tf.nn.relu(x)
        else:
            # calculate embeddings
            emb = self.embedding(x)
            # find no. of non-zero tags (non-padded)
            x = tf.cast(x != 0, dtype=tf.float32)
            x = tf.convert_to_tensor(x, dtype=tf.float32)
            x = tf.expand_dims(x, -1)
            x = tf.reduce_sum(x, axis=2)
            tags = tf.reduce_sum(emb, axis=2)
            # calculate the average for each tags
            tags = tags / x
            # take only each 10th embedding value -> (batch_size, 12, emb_dim/10)
            x = tags[:, :, ::self.emb_divider]
            x = tf.convert_to_tensor(x, dtype=tf.float32)
            # flatten the vector -> (batch_size, 12 * emb_dim/10)
            x = self.flat(x)

        return x


if __name__ == "__main__":
    print("encoder")
    # encoder