import tensorflow as tf


class RNN_Decoder(tf.keras.Model):

    def __init__(self, embedding_dim, units, vocab_size, cell_type, dropout=0.0, reg=None, rnn_layers_no=1,
                 embeddings=None, embeddings_trainable=False):
        super(RNN_Decoder, self).__init__()
        self.units = units
        self.rnn_layers = rnn_layers_no
        if embeddings is None:
            self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        else:
            # self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
            self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim, weights=[embeddings],
                                                       trainable=embeddings_trainable)
        self.cell_type = cell_type
        if self.cell_type == 'gru':
            self.rnn_cells = [tf.keras.layers.GRU(self.units,
                                                  return_sequences=True,
                                                  return_state=True,
                                                  recurrent_initializer='glorot_uniform',
                                                  dropout=dropout,
                                                  kernel_regularizer=reg,
                                                  ) for _ in range(1, self.rnn_layers)]

            self.rnn_cell2 = tf.keras.layers.GRU(self.units,
                                                 return_sequences=False,
                                                 return_state=True,
                                                 recurrent_initializer='glorot_uniform',
                                                 kernel_regularizer=reg)
        else:
            self.rnn_cells = [tf.keras.layers.LSTM(self.units,
                                                   return_sequences=True,
                                                   return_state=True,
                                                   recurrent_initializer='glorot_uniform',
                                                   dropout=dropout,
                                                   kernel_regularizer=reg,
                                                   ) for _ in range(1, self.rnn_layers)]

            self.rnn_cell2 = tf.keras.layers.LSTM(self.units,
                                                  return_sequences=False,
                                                  return_state=True,
                                                  recurrent_initializer='glorot_uniform',
                                                  kernel_regularizer=reg)

        self.fc1 = tf.keras.layers.Dense(self.units)
        self.fc2 = tf.keras.layers.Dense(vocab_size)

    def call(self, x, features, hidden):

        # x shape after passing through embedding == (batch_size, 1, embedding_dim)
        x = self.embedding(x)

        # x shape after concatenation == (batch_size, 1, embedding_dim + hidden_size)
        x = tf.concat([tf.expand_dims(features, 1), x], axis=-1)

        # passing the concatenated vector to the GRU or LSTM
        for i, cell in enumerate(self.rnn_cells):
            x = cell(x)

        rnn_output = self.rnn_cell2(x)
        x, state = rnn_output[0], rnn_output[-1]
        # shape == (batch_size, max_length, hidden_size)
        x = self.fc1(x)
        # x shape == (batch_size * max_length, hidden_size)
        # x = tf.reshape(x, (-1, x.shape[2]))
        # if self.rnn_layers == 1:
        #     x = tf.reshape(x, (-1, x.shape[2]))
        # output shape == (batch_size * max_length, vocab)
        x = self.fc2(x)

        return x, state  # for lstm diff

    def reset_state(self, batch_size):
        return tf.zeros((batch_size, self.units))


if __name__ == "__main__":
    print("decoder")

