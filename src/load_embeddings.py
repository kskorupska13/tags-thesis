from tqdm import tqdm
import numpy as np


def load_embeddings(file, skip_first=0, include_words=None):
    '''
    Load embeddings from text or .vec file.
    Input file format:
    [comments - allowed]
    word_1 x11 x12 ... x1n
    word_2 x21 x22 ... x2n
    ...
    :param file: string
        path to file with embeddings.
    :param skip_first: default = 0
        How many lines to skip at the file beggining. Useful when there ara comments/metadata at the top of the file.
    :param include_words: set or list
        Set of words which should be included in embedding dictionary. If None all words will be used.
    :return: dict
        Dictionary of pairs word:embedding.
    '''

    embeddings_dict = {}

    with open(file, "r", encoding='utf-8') as f:
        print(f)
        for _ in range(skip_first):
            print(f)
            next(f)  # skipping of some lines at the beggining

        if include_words:
            for line in tqdm(f):
                values = line.split()
                word = values[0]
                if word in include_words:
                    coefs = np.asarray(values[1:], dtype='float32')
                    embeddings_dict[word] = coefs
        else:
            for line in tqdm(f):
                values = line.split()
                word = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                embeddings_dict[word] = coefs

    return embeddings_dict


def create_embedding_matrix_from_dict(embeddings, word_indices):
    '''
    Transform dict of embeddings to matrix.
    :param embeddings: dict
        Dict of pairs word:embedding.
    :param word_indices: dict
        Dictionary from tokenizer word_index - pairs word:word_id
    :return:
    '''

    embedding_dim = len(embeddings[list(embeddings.keys())[0]])  # deducing embeddings dim
    n_rows = len(word_indices)  # vocabulary size (adding 1 in case of indexing from 1)
    embeding_matrix = np.random.normal(0, 0.5, (n_rows, embedding_dim))
    embeding_matrix[0] = np.zeros(embedding_dim)
    for word, i in word_indices.items():
        try:
            embeding_matrix[i] = embeddings[word]
        except:
            pass

    return embeding_matrix


if __name__ == "__main__":
    dict_emb = load_embeddings("D:\Karolina\Projects\embeddings/nkjp+wiki-forms-all-300-skipg-hs.txt", 1, ['<pad>'])
    print(dict_emb)