import tensorflow as tf
from datetime import datetime
import itertools
import time
import argparse
import mlflow
import mlflow.tensorflow
import sys
import pickle
from src.preprocess import *
from src.model.rnn_decoder import *
from src.model.tags_encoder import *
from src.tags_search import *
import numpy as np
import json
import language_tool_python
import re
from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
from nltk.translate.meteor_score import single_meteor_score


def map_func(tag, cap):
    '''
    A map function which casts the tags into tf.float32.
    Without it concatenation in RNN network doesn't work.
    :param tag: a single tag array
    :param cap: a single caption
    :return:
    '''
    return tf.cast(tag, tf.float32), cap


def create_dataset(tags, caps, BUFFER_SIZE, BATCH_SIZE):
    '''
    Create a tf.data.Dataset object from the data
    :param tags: tags vector
    :param caps: captions vector
    :return: tf.data.Dataset object
    '''
    dataset = tf.data.Dataset.from_tensor_slices((tags, caps))

    # # Use map to load the numpy files in parallel
    dataset = dataset.map(map_func)

    # Shuffle and batch

    dataset = dataset.shuffle(BUFFER_SIZE).batch(BATCH_SIZE)

    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset


parser = argparse.ArgumentParser()
# parser.add_argument("--batch_size", default=64, type=int, help="batch size")
parser.add_argument("--epochs", default=20, type=int, help="number of epochs")


# Find the maximum length of any caption in our dataset
def calc_max_length(tensor):
    return max(len(t) for t in tensor)


def main(argv):
    # Load data
    train_captions, tags_vector = preprocess()
    to_tokenize = '.,:;!?'
    train_captions = [re.sub(r'([' + to_tokenize + '])', r' \1', w) for w in train_captions]
    # Tokenizer
    top_k = 5000
    tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=top_k,
                                                      oov_token="<unk>",
                                                      filters='"#$%&()*+/=?@[\]^_`{|}~ ')

    # Fit tokenizer
    tokenizer.fit_on_texts(train_captions)
    train_seqs = tokenizer.texts_to_sequences(train_captions)

    tokenizer.word_index['<pad>'] = 0

    tokenizer.index_word[0] = '<pad>'
    # Create the tokenized vectors
    train_seqs = tokenizer.texts_to_sequences(train_captions)
    # Pad each vector to the max_length of the captions
    # If you do not provide a max_length value, pad_sequences calculates it automatically
    cap_vector = tf.keras.preprocessing.sequence.pad_sequences(train_seqs, padding='post')
    max_length = calc_max_length(train_seqs)
    # Save tokenizer

    artifacts_path = 'D:\Karolina\Projects\TagsThesis/artifacts/'
    print("saving tokenizer")
    with open(artifacts_path + 'tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    if top_k > len(tokenizer.index_word):
        top_k = len(tokenizer.index_word) - 1
    print(top_k)
    # # loading
    # with open('tokenizer.pickle', 'rb') as handle:
    #     tokenizer = pickle.load(handle)
    print(tokenizer.word_index)
    tags_train, tags_test, cap_train, cap_test = train_test_split(tags_vector,
                                                                  cap_vector,
                                                                  test_size=0.2,
                                                                  random_state=0)

    tags_train, tags_val, cap_train, cap_val = train_test_split(tags_train,
                                                                cap_train,
                                                                test_size=0.25,
                                                                random_state=0)

    # Here the grid could start

    batch_size = [128]#,64]  # ,64, 128]
    encoding = ['ohe']  # , 'dense']
    cell_type = ['gru']
    embedding_dim = [256]
    tags_embedding_dim = [256]
    units1 = [512]#, 512]#, 256, 128]128,
    units2 = [256, 128]
    units3 = [256, 128]
    learning_rate = [0.01]  # ,0.001]
    optimizer = ["Adam"]  # , "Adagrad", "Adadelta"]
    dropout =  [0.4]#0.0][0.4,
    regularization = ["None"]#, "L1", "L2"]
    rnn_layers_no = [2]#, 2]
    # embedding_type = [None,"fasttext","word2vec"]

    grid = list(
        itertools.product(batch_size, encoding, cell_type, embedding_dim, tags_embedding_dim, units1,
                          learning_rate,
                          optimizer, dropout, regularization, rnn_layers_no))
    best_loss = 10

    for (
            exp_num,
            (batch_size, encoding, cell_type, embedding_dim, tags_embedding_dim, units1, learning_rate,
             optimizer, dropout, regularization, rnn_layers_no)) in enumerate(grid):
        with mlflow.start_run():
            # Parameters

            BATCH_SIZE = batch_size
            BUFFER_SIZE = 500
            if dropout == 0.0:
                if regularization == "L1":
                    reg = tf.keras.regularizers.l1(0.01)
                    dropout = 0.0
                elif regularization == "L2":
                    reg = tf.keras.regularizers.l2(0.01)
                    dropout = 0.0
                else:
                    reg = None
            else:
                if regularization == "None":
                    reg = None
                else:
                    continue

            vocab_size = top_k + 1
            num_steps = len(tags_train) // BATCH_SIZE
            val_num_steps = len(tags_val) // BATCH_SIZE
            print(num_steps, val_num_steps)
            print("Experiment number {}".format(exp_num))
            mlflow.log_params({"encoding": encoding,
                               "cell type": cell_type,
                               "tags embedding size": tags_embedding_dim,
                               "units": units1,
                               # "units 2": units2,
                               # "units 3": units3,
                               "embedding dimension": embedding_dim,
                               "batch_size": BATCH_SIZE,
                               "regularization": regularization,
                               "optimizer": optimizer,
                               "learning rate": learning_rate,
                               "dropout": dropout,
                               "rnn layers": rnn_layers_no
                               })

            # Dataset
            dataset = create_dataset(tags_train, cap_train, BUFFER_SIZE, BATCH_SIZE)
            val_dataset = create_dataset(tags_train, cap_train, BUFFER_SIZE, BATCH_SIZE)

            # hidden size - units
            units = units1
            # Encoder / Decoder
            encoder = TagsEncoder(tags_embedding_dim, encoding)
            decoder = RNN_Decoder(embedding_dim, units, vocab_size, cell_type, dropout, reg,
                                  rnn_layers_no)
            optimizer_name = optimizer
            # Optimizer
            if optimizer == "Adagrad":
                optimizer = tf.keras.optimizers.Adagrad(learning_rate=learning_rate)
            elif optimizer == "Adadelta":
                optimizer = tf.keras.optimizers.Adadelta(learning_rate=learning_rate)
            else:
                optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)

            loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
                from_logits=True, reduction='none')

            # Loss plot array
            loss_plot = []

            # Loss function
            def loss_function(real, pred):
                mask = tf.math.logical_not(tf.math.equal(real, 0))
                loss_ = loss_object(real, pred)

                mask = tf.cast(mask, dtype=loss_.dtype)
                loss_ *= mask

                return tf.reduce_mean(loss_)

            def step(tags_tensor, target):
                loss = 0

                # initializing the hidden state for each batch
                # because the captions are not related from image to image
                hidden = decoder.reset_state(batch_size=target.shape[0])

                dec_input = tf.expand_dims([tokenizer.word_index['<start>']] * target.shape[0], 1)

                with tf.GradientTape() as tape:
                    features = encoder(tags_tensor)
                    print(features)
                    #  features = tags_tensor
                    #  features
                    # print(target.shape[1])
                    for i in range(1, target.shape[1]):
                        # passing the features through the decoder
                        predictions, hidden = decoder(dec_input, features, hidden)

                        loss += loss_function(target[:, i], predictions)

                        # using teacher forcing
                        dec_input = tf.expand_dims(target[:, i], 1)

                return loss, tape

            @tf.function
            def train_step(tags_tensor, target):

                loss, tape = step(tags_tensor, target)

                total_loss = (loss / int(target.shape[1]))

                trainable_variables = decoder.trainable_variables  # encoder.trainable_variables +

                gradients = tape.gradient(loss, trainable_variables)

                optimizer.apply_gradients(zip(gradients, trainable_variables))

                return loss, total_loss

            @tf.function
            def validation_step(tags_tensor, target):

                loss, tape = step(tags_tensor, target)
                total_loss = (loss / int(target.shape[1]))

                return loss, total_loss

            # args = parser.parse_args(argv[1:])
            epoch = 0
            # EPOCHS = 20
            max_epochs = 30
            min_delta = 0.005
            last_ev_loss = 10000000
            early_stop = False
            print("Starting training")
            while epoch < max_epochs and early_stop == False:
                # for epoch in range(start_epoch, max_epochs):
                start = time.time()
                total_loss = 0
                total_val_loss = 0

                for (batch, (tags_tensor, target)) in enumerate(
                        dataset):  # Could be changed so the tf.dataset is not used
                    batch_loss, t_loss = train_step(tags_tensor, target)

                    # print(batch_loss)
                    total_loss += t_loss

                    if batch % 10 == 0:
                        print('Epoch {} Batch {} Loss {:.4f}'.format(
                            epoch + 1, batch, batch_loss.numpy() / int(target.shape[1])))
                # storing the epoch end loss value to plot later
                e_loss = total_loss / num_steps
                loss_plot.append(e_loss)

                # validation
                for tags_tensor, target in val_dataset:
                    v_loss, tv_loss = validation_step(tags_tensor, target)
                    total_val_loss += tv_loss
                ev_loss = total_val_loss / val_num_steps
                # checking if there is an improvement larger than min delta
                delta = last_ev_loss - ev_loss

                if delta > min_delta:
                    last_ev_loss = ev_loss
                else:
                    early_stop = True
                    mlflow.log_param("epochs", epoch + 1)
                # increment epoch
                if epoch == max_epochs-1:
                    mlflow.log_param("epochs", epoch + 1)
                epoch += 1
                mlflow.log_metrics({'loss': e_loss.numpy()}, step=epoch)
                mlflow.log_metrics({'validation loss': ev_loss.numpy()}, step=epoch)
                print('Epoch {} Loss {:.6f} Validation loss {:.6f}'.format(epoch,
                                                                           e_loss, ev_loss))
                print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))

            # Saving model in the artifacts directory
            if best_loss > ev_loss:
                best_loss = ev_loss
                noww = datetime.now()
                models_dir = "D:\\Karolina\\Projects\\TagsThesis\\artifacts\\models/"
                enc_name = "Enc-" + noww.strftime("%d%m%Y") + "/" + str(exp_num) + "/"
                dec_name = "Dec-" + noww.strftime("%d%m%Y") + "/" + str(exp_num) + "/"
                enc_path = os.path.join(models_dir, enc_name)
                dec_path = os.path.join(models_dir, dec_name)
                print('Saving model...')
                tf.saved_model.save(encoder, enc_path)
                mlflow.log_artifact(enc_path, 'encoder')
                decoder.save_weights(dec_path)
                mlflow.log_artifact(dec_path, 'decoder')

            # Evaluation
            # 1) example description
            # 2) language tool
            # 3) bleu
            tool = language_tool_python.LanguageTool('pl')

            def evaluate(tags):
                '''
                Generate a description from tags using trained model
                :param tags: previously prepared tags to generate caption
                :return: a string - resulting caption from tags
                '''
                features = encoder(tags)

                hidden = decoder.reset_state(batch_size=1)

                dec_input = tf.expand_dims([tokenizer.word_index['<start>']], 0)
                result = []

                for i in range(max_length):
                    predictions, hidden = decoder(dec_input, features, hidden)

                    predicted_id = tf.random.categorical(predictions, 1)[0][0].numpy()
                    result.append(tokenizer.index_word[predicted_id])

                    if tokenizer.index_word[predicted_id] == '<end>':
                        return result

                    dec_input = tf.expand_dims([predicted_id], 0)

                return result

            def map_func2(tag):
                '''
                Change the type for evaluation
                :param tag: a matrix of tags for one sample
                :return: tags after type casting
                '''
                return tf.cast(tag, tf.float32)

            def prepare_tags(tags_test):
                '''
                Preparing the tags for evaluation.
                :param tags_test: a matrix of tags of one sample
                :return: prepared tags
                '''
                tags_test = tags_test.reshape(1, -1)
                tags = map_func2(tags_test)
                return tags

            # Example description to save
            description_example = {'tags': 'a',
                                   'real_description': 'b',
                                   'predicted_description': 'c'}
            # choose random one
            rid = np.random.randint(0, len(tags_test))
            example = tags_test[rid]
            # reshape needed !!!!
            # tags_test = tags_test.reshape(1, -1)
            real_caption = ' '.join([tokenizer.index_word[i] for i in cap_test[rid] if i not in [0]])
            # change to float32
            features = prepare_tags(example)

            # Evaluation
            result = evaluate(features)
            # print(tags_test[rid])
            # print('Real Caption:', real_caption)
            description_example['real_description'] = real_caption
            # print('Prediction Caption:', ' '.join(result))
            description_example['predicted_description'] = ' '.join(result)
            artifacts_path = 'D:\\Karolina\\Projects\\TagsThesis/artifacts/'
            with open(artifacts_path + 'ohe_enc.pickle', 'rb') as handle:
                enc = pickle.load(handle)
            tags_inverse = enc.inverse_transform([tags_test[rid], ])
            print(tags_inverse)
            # description_example['tags'] = ' '.join(tags_inverse[0])
            with open('generated_descriptions_example.txt', 'w', encoding='utf-8') as outfile:
                json.dump(description_example, outfile, ensure_ascii=False)
            mlflow.log_artifact('generated_descriptions_example.txt')
            os.remove('generated_descriptions_example.txt')

            # Go through the whole test dataset and calculate
            # a) Average number of the mistakes in a description
            # b) The percent of the correct results
            # c) Average BLEU
            # d) Save the generated description and correct one in csv

            def evaluation(tags_test):
                length = len(tags_test)
                print("Length of dataset:")
                total_errors = 0
                correct_captions = 0
                # avg_tags = 0
                total_words = 0
                total_real = 0
                total_tags_found = 0
                predicted = []
                real = []
                all_tags = []
                smoothie = SmoothingFunction().method4
                with open(artifacts_path + 'ohe_enc.pickle', 'rb') as handle:
                    ohe_enc = pickle.load(handle)

                print(tags_vector[0])

                BLEU = 0

                for idx, tags in enumerate(tags_test):
                    # save tags
                    tags_names = enc.inverse_transform([tags])

                    all_tags.append(tags_names)
                    # prepare for evaluation
                    tags = prepare_tags(tags)
                    # generating a caption
                    result = evaluate(tags)

                    # get the real caption
                    real_caption = [tokenizer.index_word[i].replace(u'\xa0', u'') for i in cap_test[idx] if
                                    i not in [0]]
                    # print(real_caption)
                    # getting rid of <start> <end> token
                    result = result[:-1]
                    real_caption = real_caption[1:-1]
                    # print(real_caption ,'/n', result)

                    # calculate BLEU
                    # if optimizer_name == "Adam":
                    #     score = sentence_bleu(real_caption, result, weights=(1, 0, 0, 0), smoothing_function=smoothie)
                    #     BLEU += score
                    # print(real_caption,result)

                    # creating a string with capital letter
                    sentence = ' '.join(result)

                    total_words += len(re.findall(r'\w+', sentence))

                    # fix the interpunction signs
                    sentence = re.sub(r' ([' + to_tokenize + '])', r'\1', sentence)
                    to_capitalize = re.split('([.!?] *)', sentence)
                    sentence = ''.join([i.capitalize() for i in to_capitalize])

                    # print(sentence)
                    # add to predicted list
                    predicted.append(sentence)

                    # add the real caption
                    real_sentence = ' '.join(real_caption)
                    real.append(real_sentence)

                    # check no of tags
                    no_of_tags = tags_search(tags_names, sentence)
                    no_of_tags_real  = tags_search(tags_names, real_sentence)
                    # print(no_of_tags)
                    total_tags_found += no_of_tags
                    total_real += no_of_tags_real
                    # check description errors
                    matches = tool.check(sentence)
                    errors = len(matches)
                    # print(errors)
                    if errors == 0:
                        correct_captions += 1
                        # print(result)
                    elif not sentence:
                        # empty += 1
                        length = length - 1
                    else:
                        total_errors += errors

                avg_errors = total_errors / length
                correctness = correct_captions / length
                avg_words = total_words / length
                avg_bleu = BLEU / length

                avg_tags = total_tags_found / length
                descriptions_df = pd.DataFrame({"tags": avg_tags, "predicted": predicted, "real": real})
                descriptions_df.to_csv(
                    artifacts_path + "Descriptions" + noww.strftime("%d%m%Y") + "experiment" + str(
                        exp_num) + "_{}_{}_{}_{}".format(str(rnn_layers_no), str(units), regularization,cell_type) + ".csv",
                    encoding='utf-8-sig',
                    index=False, header=True)
                print(total_real)
                return avg_errors, correctness, avg_words, avg_bleu, avg_tags

            # Evaluate the whole test set
            avg_errors, correctness, avg_words, avg_bleu, avg_tags = evaluation(tags_test)

            mlflow.log_metrics({"avg_errors": avg_errors,
                                "correctness": correctness,
                                "word count": avg_words,
                                "average BLEU": avg_bleu,
                                "tags number": avg_tags})



if __name__ == "__main__":
    main(sys.argv)
    # print("train")
    # print(tags_val.shape, tags_train.shape, len(cap_val), len(cap_train))
    # test_dataset = create_dataset(tags_train, cap_train)
    # print(test_dataset)
    # run(test_dataset)
