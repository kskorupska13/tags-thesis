import os
import pandas as pd
from glob import glob
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.preprocessing import OneHotEncoder
import pickle
import re
import ast

# Get data from file and preprocess them so they are ready for encoding

def get_data():
    '''
    Preprocess shoes dataset
    :return: tags , captions
    '''
    data_path = 'D:\\Karolina\\Projects\\TagsThesis\\data'
    # print(data_path)
    csv_files = []
    if os.path.exists(data_path):
        csv_files = [f for f in glob(data_path + "**/*.csv", recursive=True)]
    # print(csv_files)
    # Shoes
    # tags
    print(csv_files[1],csv_files[0])
    tags_df = pd.read_csv(csv_files[1])
    tags_df = tags_df.fillna('<none>')
    # shoesdf = tags_df.drop(tags_df[tags_df['Rodzaj obcasa'] == '<none>'].index)
    des_df = pd.read_csv(csv_files[0])
    d_df = pd.DataFrame(des_df['description'])

    # d_df = d_df.drop_duplicates()
    # sh_df = sh_df.drop_duplicates()
    merged_df = pd.concat([tags_df, d_df], axis=1)

    merged_df = merged_df.drop_duplicates(subset=['description'])
    merged_df = merged_df.drop(merged_df[merged_df['Rodzaj obcasa'] == '<none>'].index)
    print(merged_df.isnull().sum())
    ddf = pd.DataFrame(merged_df['description'])
    tdf = merged_df.drop(['description'], axis=1)

    return tdf, ddf

def prepare_tag(tag):
    '''
    Prepare the tag to be a list of words included in a tag.
    It mainly divides connected expressions. Ex." x/y/z" -> "x,y,z"
    :param tag: a tag from dataframe
    :return: list of all words in tag
    '''
    if tag != '<none>':
        # delete "/" if appplies
        text, no_of_replaces = re.subn(r'[+"/"+]', r' ', tag)

        if no_of_replaces != 0:
            text = text.split()
            new_tags = []
            # print('text: ', text)
            for t in text:
                # seperate capitalized words

                t = re.sub(r"(\w)([A-Z])", r"\1 \2", t)

                t = t.lower()
                t = t.split()
                new_tags.extend(t)

                # print(t)

            # print(type(new_tags))
            return new_tags
        tag = re.sub(r"(\w)([A-Z])", r"\1 \2", tag)
    return [tag.lower()]

def sequence(tags):
    '''
    Transform the tags to lists of all words.
    :param tags: A row of tags from datframe
    :return: list of lists of words in the each tag
    '''
    new_tags = []
    for tag in tags:

        if isinstance(tag, list):
            tags_list = []
            for t in tag:
                new_tag = prepare_tag(t)
                tags_list.extend(new_tag)
            new_tags.append(tags_list)
        else:
            new_tag = prepare_tag(tag)
            new_tags.append(new_tag)
    return new_tags

def ohe_tags(tags):
    '''
    Encode tags with ohe from scikit-learn library
    :param tags: Tags row
    :return: Ohe matrix
    '''
    enc = OneHotEncoder(sparse=False)
    tags_ohe = enc.fit_transform(tags)
    artifacts_path = 'D:\Karolina\Projects\TagsThesis/artifacts/'
    print("saving ohe encoder")
    with open(artifacts_path + 'ohe_enc.pickle', 'wb') as handle:
        pickle.dump(enc, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return tags_ohe



def cap_prep(caps):
    '''
    Add <start> and <end> tokens to the captions
    :param caps:
    :return:
    '''
    # Store captionsin vectors
    all_captions = []
    # all_img_name_vector = []

    for desc in caps['description']:
        caption = '<start> ' + desc + ' <end>'

        all_captions.append(caption)
    return all_captions



def preprocess(ohe = True):
    '''
    Reads data, preprocess it (ohe encoding or list of words), shuffles and takes 5000 samples.
    :return: captions vector, tags vector
    '''
    t_df, c_df = get_data()
    if ohe:
        tags = ohe_tags(t_df)
    else:
        tags = t_df.to_numpy()
        tags = [sequence(tag) for tag in tags]
        print(tags[0])
    caps = cap_prep(c_df)
    cap_vector, tags_vector = shuffle(caps,
                                      tags,
                                      random_state=1)
    num_examples = 5000
    cap_vector = cap_vector[:num_examples]
    tags_vector = tags_vector[:num_examples]

    return cap_vector, tags_vector


if __name__ == "__main__":
    print("preprocess")
    a, b = get_data()
    c_t, t_t = preprocess()
    # print(t_t)
    # print(c_t)
    print(len(t_t), len(c_t))
    print(b.head())