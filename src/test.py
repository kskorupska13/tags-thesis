import language_tool_python as ltp
import re
import pickle
from src.load_embeddings import *
from src.model.tags_encoder import *
import numpy as np
# from src.preprocess import *
from nltk.translate.meteor_score import single_meteor_score
# from src.tags_search import *
import itertools
# import spacy
# nlp = spacy.load('pl_spacy_model') # or spacy.load('pl_spacy_model_morfeusz')

# # List the tokens including their lemmas and POS tags
# doc = nlp("Granice mojego języka oznaczają granice mojego świata") # ~Wittgenstein
# for token in doc:
#     print(token.text, token.lemma_, token.tag_)


# from sklearn.preprocessing import OneHotEncoder
# # import tensorflow as tf
# #
# # tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=2000,
# #                                                   oov_token="<unk>",
# #                                                   filters='#$%&()*+')
# to_exclude = '!"#$%&()*+-/:;<=>@[\\]^_`{|}~\t\n'
# to_tokenize = '.,:;!?'
#
# text = "tomorrow, will be. cold?"
# text = re.sub(r'([' + to_tokenize + '])', r' \1', text)
# print(text)
# text = re.sub(r' ([' + to_tokenize + '])', r'\1', text)
# p = re.compile(r'((?<=[\.\?!]\s)(\w+)|(^\w+))')
#
# rtn = re.split('([.!?] *)', text)
# final = ''.join([i.capitalize() for i in rtn])
artifacts_path = 'D:\Karolina\Projects\TagsThesis/artifacts/'
# print(final)
#
# train_captions, tags_vector = preprocess()
#
with open(artifacts_path+'ohe_enc.pickle', 'rb') as handle:
    enc = pickle.load(handle)

with open(artifacts_path+'tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)

tags_test = [['Klapki', 'Brak/Płaski', 'Wsuwane', 'Średni', '<none>', 'Letnie', '<none>',
                  'Czarny', 'Skóra/Ekoskóra/ImitacjaSkóry', 'Neutralny', '<none>',
                  ['Zamszowe', 'Nubuk/SkóraZamszowa']]]
new_tags = []

def prepare_tag(tag):
    if tag != '<none>':
        # delete "/" if appplies
        text, no_of_replaces = re.subn(r'[+"/"+]', r' ', tag)

        if no_of_replaces != 0:
            text = text.split()
            new_tags = []

            for t in text:
                # seperate capitalized words
                t = re.sub(r"(\w)([A-Z])", r"\1 \2", t)
                t = t.lower()
                t = t.split()
                new_tags.extend(t)
                # print(t)
            return new_tags
    return [tag.lower()]




for tag in tags_test[0]:

    if isinstance(tag, list):
        tags_list = []
        for t in tag:
            new_tag = prepare_tag(t)
            tags_list.extend(new_tag)
        new_tags.append(tags_list)
    else:
        new_tag = prepare_tag(tag)
        new_tags.append(new_tag)

print(new_tags)
last = len(tokenizer.word_index)

tokenizer.word_index['<none>'] = last
tokenizer.index_word[last] = '<none>'


tags_tok = tokenizer.texts_to_sequences(new_tags)
print(tags_tok)
embedding_dim = 100
embedding_type = "cbow"



vocab_size = len(tokenizer.index_word)
print(vocab_size)
print(tokenizer.index_word)
print(tags_tok)

tags = tf.keras.preprocessing.sequence.pad_sequences(tags_tok, padding='post')
print(tags)
x = tf.cast(tags != 0, dtype=tf.float32)

# x = tf.expand_dims(x, -1)
print(x)
embeddings_dict = load_embeddings(
                "D:\Karolina\Projects/embeddings/nkjp+wiki-forms-all-{}-{}-hs.txt".format(
                    str(embedding_dim), embedding_type),
                skip_first=1,
                include_words=set(tokenizer.word_index.keys()))
embeddings = create_embedding_matrix_from_dict(embeddings_dict, tokenizer.word_index)
# print(embeddings.size)
# print(embeddings[3474])
embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim, weights=[embeddings],
                                                       trainable=False)

features = embedding(tags)
print(features)
# x = tf.reduce_sum(x,1,keepdims=True)
# features = tf.reduce_sum(features,1)

mean_tags = tf.reduce_sum(features,1)
print(mean_tags)
mean_tags = mean_tags/tf.reduce_sum(x,1,keepdims=True)
# reshape = tf.keras.layers.Flatten()(mean_tags)
tags = mean_tags[:,::10]
print(tags)
# ----> here
# print(mean_tags)
# encoder = TagsEncoder(embedding_dim, 'emb', vocab_size, embeddings, False)
# # tags_tensor = tf.convert_to_tensor(tags_tok, dtype=tf.float32)
# features = encoder(np.asarray(tags_tok).astype('float32'))
#np.asarray(x).astype('float32')
# print(tags_vector[1])
# tags = enc.inverse_transform([tags_vector[1]])
# print(tags)
# print(enc.categories_)


# all_tags = np.concatenate(enc.categories_)
# print(all_tags)
# tokenizer.fit_on_texts(text)
# print(tokenizer.word_index)

# tool = ltp.LanguageTool('pl')
# to_check = "Szare mokasyny, że płaskim obcasie i chwostami na zamek błyskawiczny"
# matches = tool.check(to_check)
# print(len(matches))
# print(round(single_meteor_score('buty wsuwane. koloru czerwonego, na rzep', 'czerwone buty na rzep'),4))
# print(matches)
