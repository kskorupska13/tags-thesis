import tensorflow as tf
import numpy as np
import pickle
import src.train

artifacts_path = 'D:\Karolina\Projects\TagsThesis/artifacts/'

# Load tokenizer
with open('tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)

# Load models
# encoder

# decoder
encoder = TagsEncoder(tags_embedding_dim, encoding)
decoder = RNN_Decoder(embedding_dim, units, vocab_size, cell_type)

encoder.load_weights(save_path)

def evaluate(tags):
    features = encoder(tags)
    hidden = decoder.reset_state(batch_size=1)

    dec_input = tf.expand_dims([tokenizer.word_index['<start>']], 0)
    result = []

    for i in range(src.train.max_length):
        predictions, hidden = decoder(dec_input, features, hidden)

        predicted_id = tf.random.categorical(predictions, 1)[0][0].numpy()
        result.append(tokenizer.index_word[predicted_id])

        if tokenizer.index_word[predicted_id] == '<end>':
            return result

        dec_input = tf.expand_dims([predicted_id], 0)

    return result


def map_func2(tag):
    return tf.cast(tag, tf.float32)


# # encode the validation set set -> could be done earlier
# tags_val = encoder(tags_val,'ohe')

# choose random one
rid = np.random.randint(0, len(src.train.tags_val))
tags_test = src.train.tags_val[rid]
# reshape needed !!!!
tags_test = tags_test.reshape(1, -1)
real_caption = ' '.join([tokenizer.index_word[i] for i in src.train.cap_val[rid] if i not in [0]])
# change to float32
features = map_func2(tags_test)
# Evaluation
result = evaluate(features)
print(src.train.tags_val[rid])
print('Real Caption:', real_caption)

print('Prediction Caption:', ' '.join(result))
