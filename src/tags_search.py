import re
import spacy

nlp = spacy.load('pl_spacy_model')  # or spacy.load('pl_spacy_model_morfeusz')


def prepare_tag(tag):
    if tag != '<none>':
        # delete "/" if appplies
        text, no_of_replaces = re.subn(r'[+"/"+]', r' ', tag)

        if no_of_replaces != 0:
            # if there were some replacements -> we need to soerate tags
            # print(text)
            text = text.split()
            new_tags = []

            for t in text:
                # print(t)
                # seperate capitalized words
                t = re.sub(r"(\w)([A-Z])", r"\1 \2", t)

                new_tags.append(t.lower())
                # print(t)

            return new_tags
        return [tag.lower()]


def get_lemmas(tag):
    '''
    Returns lemmatized string
    :param tag: a string of 1 or more words
    :return: lemmatized string
    '''
    tag_l = nlp(tag)
    # print(len(tag_l))
    lemmas = []  # all lemmatized words in tag
    # print(len(tag_l))
    for i in range(len(tag_l)):
        #     print(w.lemma_)
        lemmas.append(str(tag_l[i].lemma_))
    # print(lemmas)
    return ' '.join(lemmas)


def tags_search(tags_list, desc, ohe = True):
    no_of_tags = 0
    tags_in_desc = []

    # words from tags list
    words_list = []
    if ohe:
        tags = tags_list[0]
        # Get all tags from the tags list
        for tag in tags:
            if tag != '<none>':
                if isinstance(tag, list):
                    tags_list = []
                    for t in tag:
                        new_tag = prepare_tag(t)
                        tags_list.extend(new_tag)
                    words_list.append(tags_list)
                else:
                    new_tag = prepare_tag(tag)
                    words_list.append(new_tag)
    else:
        words_list = tags_list
    # print(len(words_list))
    # print(words_list)
    # all_tags = ' '.join(words_list)

    # use spacy for tags
    # doc = nlp(all_tags)
    #
    # lemmatized tags
    # tags_lemma = []
    for tag in words_list:
        new_strings = []
        for s in tag:
            lemma_tag = get_lemmas(s)
            # print(lemma_tag)
            new_strings.append(lemma_tag)
        tag.extend(new_strings)
    # print(words_list)
    # print(tags_no)

    # spacy for description
    desc_nlp = get_lemmas(desc)
    # print(desc_nlp)
    # choose only distinct words

    # check tags appearance
    for tag in words_list:
        for s in tag:
            if s in desc_nlp:
                # print(s)
                no_of_tags += 1
                break
            elif s in desc:
                # print(s)
                no_of_tags += 1
                break

    # for token in desc_nlp:
    #     # print(str(token.lemma_))
    #     if str(token.lemma_) in tags_lemma:
    #         tags_in_desc.append(str(token.lemma_))
    #         # print(token.lemma_)
    #         #no_of_tags += 1
    # print(tags_in_desc)
    # no_of_tags = len(set(tags_in_desc))

    # print(words_list)
    return no_of_tags


if __name__ == "__main__":
    tags_test = [['Klapki', 'Brak/Płaski', 'Wsuwane', 'Średni', '<none>', 'Letnie', '<none>',
                  'Czarny', 'Skóra/Ekoskóra/ImitacjaSkóry', 'Neutralny', '<none>',
                  ['Zamszowe', 'Nubuk/SkóraZamszowa']]]

    description = "Czarne czółenka czarnym na słupku, wkładka ze czółenka na średnim obcasie niewielka, wsuwane, z imitacji."
    print(tags_search(tags_test, description))


