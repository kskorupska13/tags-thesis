import tensorflow as tf
from datetime import datetime
import itertools
import time
import argparse
import mlflow
import mlflow.tensorflow
import sys
import pickle
from src.preprocess import *
from src.model.rnn_decoder import *
from src.model.tags_encoder import *
from src.tags_search import *
import numpy as np
import json
import language_tool_python
import re
from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
from nltk.translate.meteor_score import single_meteor_score
from src.load_embeddings import *


def uniq_chain(*args, **kwargs):
    seen = set()
    for x in itertools.chain(*args, **kwargs):
        for i in x:
            if i in seen:
                continue
            seen.add(i)
            print(i)
            yield i


def map_func(tag, cap):
    '''
    A map function which casts the tags into tf.float32.
    Without it concatenation in RNN network doesn't work.
    :param tag: a single tag array
    :param cap: a single caption
    :return:
    '''
    return tf.cast(tag, tf.float32), cap


def create_dataset(tags, caps, BUFFER_SIZE=None, BATCH_SIZE=None):
    '''
    Create a tf.data.Dataset object from the data
    :param tags: tags vector
    :param caps: captions vector
    :return: tf.data.Dataset object
    '''
    dataset = tf.data.Dataset.from_tensor_slices((tags, caps))

    # # Use map to load the numpy files in parallel
    dataset = dataset.map(map_func)

    # Shuffle and batch
    if BUFFER_SIZE is not None:
        dataset = dataset.shuffle(BUFFER_SIZE)
    if BATCH_SIZE is not None:
        dataset = dataset.batch(BATCH_SIZE)

    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset


parser = argparse.ArgumentParser()
# parser.add_argument("--batch_size", default=64, type=int, help="batch size")
parser.add_argument("--epochs", default=20, type=int, help="number of epochs")


# Find the maximum length of any caption in our dataset
def calc_max_length(tensor):
    return max(len(t) for t in tensor)


def main(argv):
    # Load data
    train_captions, tags_vector = preprocess(ohe=False)

    # Prepare captions
    to_tokenize = '.,:;!?'
    train_captions = [re.sub(r'([' + to_tokenize + '])', r' \1', w).replace(u'\xa0', u' ') for w in train_captions]

    # Tokenizer
    top_k = 5000
    tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=top_k,
                                                      oov_token="<unk>",
                                                      filters='"635#$%&()*+/=?@[\]^_`{|}~\n')

    # Fit tokenizer

    print(tags_vector[0])
    flat_tags = list(uniq_chain(*tags_vector))
    print("flat_tags")
    print(flat_tags)
    print("train captions")

    # -> if we want to add words from tags
    tags_captions = []
    tags_captions.extend(train_captions)
    tags_captions.extend(flat_tags)
    # print(tags_captions)
    tokenizer.fit_on_texts(tags_captions)
    train_seqs = tokenizer.texts_to_sequences(train_captions)

    tokenizer.word_index['<pad>'] = 0
    tokenizer.index_word[0] = '<pad>'
    # Create the tokenized vectors
    train_seqs = tokenizer.texts_to_sequences(train_captions)
    # Pad each vector to the max_length of the captions
    # If you do not provide a max_length value, pad_sequences calculates it automatically
    cap_vector = tf.keras.preprocessing.sequence.pad_sequences(train_seqs, padding='post')
    max_length = calc_max_length(train_seqs)
    # last = len(tokenizer.word_index)
    # tokenizer.word_index['<none>'] = last
    # tokenizer.index_word[last] = '<none>'
    # Tokenize tags
    print(tags_vector[0])
    tags_vector = [
        tf.keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(tags), maxlen=4, padding='post') for
        tags in tags_vector]
    print(tags_vector[0])
    test = tags_vector[0]
    # new_test = []
    # # for row in test:
    # #
    # #     new_row = [tokenizer.index_word[i].replace(u'\xa0', u'') for i in row if
    # #                 i not in [0]]
    # #     new_test.append(new_row)
    #
    # print(new_test)
    # Save tokenizer
    artifacts_path = 'D:\Karolina\Projects\TagsThesis/artifacts/'
    print("saving tokenizer")

    # # loading
    # with open('tokenizer.pickle', 'rb') as handle:
    #     tokenizer = pickle.load(handle)
    print(tokenizer.word_index)

    # Train, validation and test dataset split

    tags_train, tags_test, cap_train, cap_test = train_test_split(tags_vector,
                                                                  cap_vector,
                                                                  test_size=0.2,
                                                                  random_state=0)

    tags_train, tags_val, cap_train, cap_val = train_test_split(tags_train,
                                                                cap_train,
                                                                test_size=0.25,
                                                                random_state=0)

    # Here the grid could start
    tags_dimension_divider = [
        10]  # In embedding encoder the dimension is divided by this number to do less computations
    batch_size = [128]  # ,64, 128]
    encoding = ['emb']  # , 'dense']

    cell_type = ['lstm']  # , 'gru']
    embedding_dim = [300]
    units = [256]
    # units2 = [256, 128]
    # units3 = [256, 128]
    learning_rate = [0.01]  # ,0.001]
    optimizer = ["Adam"]  # , "Adagrad", "Adadelta"]
    dropout = [0.4,0.2]  # , 0.6]
    regularization = ["None"]  # , "L1", []
    rnn_layers_no = [2]
    embedding_type = ["skipg"]  # , ]
    embeddings_trainable = [False,True]
    embedding_name = ["fasttext", "word2vec"]

    embedding_grid = list(itertools.product(embedding_type, embedding_name, embedding_dim))

    grid = list(
        itertools.product(batch_size, encoding, cell_type, units,
                          learning_rate,
                          optimizer, dropout, regularization, rnn_layers_no, embeddings_trainable,
                          tags_dimension_divider))
    best_loss = 10

    for (embedding_no, (embedding_type, embedding_name, embedding_dim)) in enumerate(embedding_grid):

        print("{} {} {}".format(embedding_type, embedding_name, str(embedding_dim)))
        # Embeddings loading
        if embedding_name is None:
            embeddings = None
        elif embedding_name is "word2vec":
            embeddings_dict = load_embeddings(
                "D:\Karolina\Projects/embeddings/nkjp+wiki-forms-all-{}-{}-hs.txt".format(
                    str(embedding_dim), embedding_type),
                skip_first=1,
                include_words=set(tokenizer.word_index.keys()))
            embeddings = create_embedding_matrix_from_dict(embeddings_dict, tokenizer.word_index)
        else:
            embeddings_dict = load_embeddings(
                "D:\Karolina\Projects/embeddings/kgr10.plain.{}.dim{}.neg10.vec".format(
                    embedding_type, str(embedding_dim)),
                skip_first=1,
                include_words=set(tokenizer.word_index.keys()))
            embeddings = create_embedding_matrix_from_dict(embeddings_dict, tokenizer.word_index)

        for (
                exp_num,
                (batch_size, encoding, cell_type, units, learning_rate,
                 optimizer, dropout, regularization, rnn_layers_no, embeddings_trainable,
                 tags_dimension_divider)) in enumerate(grid):

            with mlflow.start_run():

                # Parameters
                BATCH_SIZE = batch_size
                BUFFER_SIZE = 500
                if dropout == 0.0:
                    if regularization == "L1":
                        reg = tf.keras.regularizers.l1(0.01)
                        dropout = 0.0
                    elif regularization == "L2":
                        reg = tf.keras.regularizers.l2(0.01)
                        dropout = 0.0
                    else:
                        reg = None
                else:
                    if regularization == "None":
                        reg = None
                    else:
                        continue

                vocab_size = len(tokenizer.index_word)
                print(vocab_size)
                num_steps = len(tags_train) // BATCH_SIZE
                val_num_steps = len(tags_val) // BATCH_SIZE
                print(num_steps, val_num_steps)
                print("Experiment number {}".format(exp_num))
                mlflow.log_params({"encoding": encoding,
                                   "cell type": cell_type,
                                   "tags embedding size": embedding_dim,
                                   "units": units,
                                   "embedding": embedding_name,
                                   "embedding type": embedding_type,
                                   "embedding dimension": 256,
                                   "embedding_trainable": embeddings_trainable,
                                   "batch_size": BATCH_SIZE,
                                   "regularization": regularization,
                                   "optimizer": optimizer,
                                   "learning rate": learning_rate,
                                   "dropout": dropout,
                                   "rnn layers": rnn_layers_no,
                                   "enc embedding dim divider": tags_dimension_divider
                                   })

                # Dataset
                dataset = create_dataset(tags_train, cap_train, BUFFER_SIZE, BATCH_SIZE)
                val_dataset = create_dataset(tags_val, cap_val, BUFFER_SIZE, BATCH_SIZE)
                test_dataset = create_dataset(tags_test, cap_test)
                # hidden size - units

                # Encoder / Decoder
                encoder = TagsEncoder(embedding_dim, encoding, vocab_size, embeddings)
                # For varying encoder tags embedding dim
                # I will use decoder emb dim = 256, because 100 could be too small
                decoder = RNN_Decoder(256, units, vocab_size, cell_type, dropout, reg,
                                      rnn_layers_no)
                optimizer_name = optimizer

                # Optimizer
                if optimizer == "Adagrad":
                    optimizer = tf.keras.optimizers.Adagrad(learning_rate=learning_rate)
                elif optimizer == "Adadelta":
                    optimizer = tf.keras.optimizers.Adadelta(learning_rate=learning_rate)
                else:
                    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)

                loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
                    from_logits=True, reduction='none')

                # Loss plot array
                loss_plot = []

                # Loss function
                def loss_function(real, pred):
                    mask = tf.math.logical_not(tf.math.equal(real, 0))
                    loss_ = loss_object(real, pred)

                    mask = tf.cast(mask, dtype=loss_.dtype)
                    loss_ *= mask

                    return tf.reduce_mean(loss_)

                def step(tags_tensor, target):
                    loss = 0

                    # initializing the hidden state for each batch
                    # because the captions are not related from image to image
                    hidden = decoder.reset_state(batch_size=target.shape[0])

                    dec_input = tf.expand_dims([tokenizer.word_index['<start>']] * target.shape[0], 1)

                    with tf.GradientTape() as tape:
                        # print(tags_tensor)
                        features = encoder(tags_tensor)
                        #  features = > (batch_size, 12 * tags_embedding_dim/10)
                        # print(features)
                        for i in range(1, target.shape[1]):
                            # passing the features through the decoder
                            predictions, hidden = decoder(dec_input, features, hidden)

                            loss += loss_function(target[:, i], predictions)

                            # using teacher forcing
                            dec_input = tf.expand_dims(target[:, i], 1)

                    return loss, tape

                @tf.function
                def train_step(tags_tensor, target):

                    loss, tape = step(tags_tensor, target)

                    total_loss = (loss / int(target.shape[1]))

                    trainable_variables = decoder.trainable_variables  # encoder.trainable_variables +

                    gradients = tape.gradient(loss, trainable_variables)

                    optimizer.apply_gradients(zip(gradients, trainable_variables))

                    return loss, total_loss

                @tf.function
                def validation_step(tags_tensor, target):

                    loss, tape = step(tags_tensor, target)
                    total_loss = (loss / int(target.shape[1]))

                    return loss, total_loss

                # args = parser.parse_args(argv[1:])
                epoch = 0
                # EPOCHS = 20
                max_epochs = 30
                min_delta = 0.001
                last_ev_loss = 10000000
                early_stop = False
                print("Starting training")
                while epoch < max_epochs: # and early_stop == False:
                    # for epoch in range(start_epoch, max_epochs):
                    start = time.time()
                    total_loss = 0
                    total_val_loss = 0

                    for (batch, (tags_tensor, target)) in enumerate(
                            dataset):  # Could be changed so the tf.dataset is not used

                        batch_loss, t_loss = train_step(tags_tensor, target)

                        total_loss += t_loss

                        if batch % 10 == 0:
                            print('Epoch {} Batch {} Loss {:.4f}'.format(
                                epoch + 1, batch, batch_loss.numpy() / int(target.shape[1])))

                    # storing the epoch end loss value to plot later
                    e_loss = total_loss / num_steps
                    loss_plot.append(e_loss)

                    # validation
                    for tags_tensor, target in val_dataset:
                        v_loss, tv_loss = validation_step(tags_tensor, target)
                        total_val_loss += tv_loss
                    ev_loss = total_val_loss / val_num_steps

                    # checking if there is an improvement larger than min delta
                    delta = last_ev_loss - ev_loss

                    if delta > min_delta:
                        last_ev_loss = ev_loss
                    else:
                        early_stop = True
                        mlflow.log_param("epochs", epoch + 1)

                    # increment epoch
                    epoch += 1

                    # Metrics mlflow
                    mlflow.log_metrics({'loss': e_loss.numpy()}, step=epoch)
                    mlflow.log_metrics({'validation loss': ev_loss.numpy()}, step=epoch)
                    print('Epoch {} Loss {:.6f} Validation loss {:.6f}'.format(epoch,
                                                                               e_loss, ev_loss))
                    print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))
                    # print(encoder.summary())
                    # print(decoder.summary())
                # Saving model in the artifacts directory
                if best_loss > ev_loss:
                    best_loss = ev_loss
                    now = datetime.now()
                    models_dir = "D:\\Karolina\\Projects\\TagsThesis\\artifacts\\models/"
                    enc_name = "Enc-" + now.strftime("%d%m%Y") + "/" + str(exp_num) + "/"
                    dec_name = "Dec-" + now.strftime("%d%m%Y") + "/" + str(exp_num) + "/"
                    enc_path = os.path.join(models_dir, enc_name)
                    dec_path = os.path.join(models_dir, dec_name)
                    print('Saving model...')
                    tf.saved_model.save(encoder, enc_path)
                    mlflow.log_artifact(enc_path, 'encoder')
                    decoder.save_weights(dec_path)
                    mlflow.log_artifact(dec_path, 'decoder')

                # Evaluation
                # 1) example description
                # 2) language tool
                # 3) bleu -> not used anymore as it doesn't work well for my problem
                tool = language_tool_python.LanguageTool('pl')

                def evaluate(tags):
                    '''
                    Generate a description from tags using trained model
                    :param tags: previously prepared tags to generate caption
                    :return: a string - resulting caption from tags
                    '''
                    # print("Before encoder:",tags)
                    features = encoder(tags)
                    # print("After encoder:",features)
                    hidden = decoder.reset_state(batch_size=1)

                    dec_input = tf.expand_dims([tokenizer.word_index['<start>']], 0)
                    result = []

                    for i in range(max_length):
                        predictions, hidden = decoder(dec_input, features, hidden)

                        predicted_id = tf.random.categorical(predictions, 1)[0][0].numpy()
                        result.append(tokenizer.index_word[predicted_id])

                        if tokenizer.index_word[predicted_id] == '<end>':
                            return result

                        dec_input = tf.expand_dims([predicted_id], 0)

                    return result

                def map_func2(tag):
                    '''
                    Change the type for evaluation
                    :param tag: a matrix of tags for one sample
                    :return: tags after type casting
                    '''
                    return tf.cast(tag, tf.float32)

                def prepare_tags(tags_test):
                    '''
                    Preparing the tags for evaluation. Needs another dim.
                    :param tags_test: a matrix of tags of one sample
                    :return: prepared tags
                    '''
                    x = tf.expand_dims(tags_test, axis=0)
                    # x = tf.convert_to_tensor(x, dtype=tf.float32)
                    # tags = map_func2(tags_test)
                    return x

                # Go through the whole test dataset and calculate
                # a) Average number of the mistakes in a description
                # b) The percent of the correct results
                # c) Average BLEU
                # d) Save the generated description and correct one in csv

                def evaluation(tags_test):
                    length = len(tags_test)
                    print("Length of dataset:")
                    total_errors = 0
                    correct_captions = 0
                    # avg_tags = 0
                    total_words = 0
                    total_tags_found = 0
                    predicted = []
                    real = []
                    # all_tags = []
                    # smoothie = SmoothingFunction().method4
                    # with open(artifacts_path + 'ohe_enc.pickle', 'rb') as handle:
                    #     ohe_enc = pickle.load(handle)

                    # print(tags_vector[0])

                    BLEU = 0

                    for (idx, (tags, cap)) in enumerate(test_dataset):
                        # save tags
                        # tags_names = enc.inverse_transform([tags])

                        # inverse encode tags
                        tags_names = []
                        for row in tags.numpy():
                            # print(row)
                            new_row = [tokenizer.index_word[i].replace(u'\xa0', u'') for i in row if
                                       i not in [0]]
                            # print(new_row)
                            tags_names.append(new_row)

                        # prepare for evaluation
                        tags = prepare_tags(tags)

                        # generating a caption

                        result = evaluate(tags)

                        # get the real caption
                        real_caption = [tokenizer.index_word[i].replace(u'\xa0', u'') for i in cap.numpy() if
                                        i not in [0]]
                        # print(real_caption)
                        # getting rid of <start> <end> token
                        result = result[:-1]
                        real_caption = real_caption[1:-1]
                        # print(real_caption ,'/n', result)

                        # calculate BLEU
                        # if optimizer_name == "Adam":
                        #     score = sentence_bleu(real_caption, result, weights=(1, 0, 0, 0), smoothing_function=smoothie)
                        #     BLEU += score
                        # print(real_caption,result)

                        # creating a string with capital letter
                        sentence = ' '.join(result)

                        total_words += len(re.findall(r'\w+', sentence))

                        # fix the interpunction signs
                        sentence = re.sub(r' ([' + to_tokenize + '])', r'\1', sentence)
                        to_capitalize = re.split('([.!?] *)', sentence)
                        sentence = ''.join([i.capitalize() for i in to_capitalize])

                        # print(sentence)
                        # add to predicted list
                        predicted.append(sentence)

                        # add the real caption
                        real_sentence = ' '.join(real_caption)
                        real.append(real_sentence)
                        no_of_tags = -1
                        # check no of tags
                        no_of_tags = tags_search(tags_names, sentence, False)

                        total_tags_found += no_of_tags

                        # check description errors
                        matches = tool.check(sentence)
                        errors = len(matches)

                        if errors == 0:
                            correct_captions += 1
                            # print(result)
                        elif not sentence:
                            # empty += 1
                            length = length - 1
                        else:
                            total_errors += errors

                    avg_errors = total_errors / length
                    correctness = correct_captions / length
                    avg_words = total_words / length
                    avg_bleu = BLEU / length

                    avg_tags = total_tags_found / length
                    avg_tags_over_words = avg_tags / avg_words
                    descriptions_df = pd.DataFrame({"tags": avg_tags, "predicted": predicted, "real": real})
                    descriptions_df.to_csv(
                        artifacts_path + "Descriptions_enc_emb_" + now.strftime("%d%m%Y") + "exp" + str(
                            exp_num) + "_{}_{}_{}_".format(embedding_name, embedding_type,
                                                           str(embedding_dim)) + ".csv", encoding='utf-8-sig',
                        index=False, header=True)

                    return avg_errors, correctness, avg_words, avg_bleu, avg_tags, avg_tags_over_words

                # Evaluate the whole test set
                avg_errors, correctness, avg_words, avg_bleu, avg_tags, avg_tags_over_words = evaluation(tags_test)
                mlflow.log_metrics({"avg_errors": avg_errors,
                                    "correctness": correctness,
                                    "word count": avg_words,
                                    "average BLEU": avg_bleu,
                                    "tags number": avg_tags,
                                    "tags over words": avg_tags_over_words})


if __name__ == "__main__":
    main(sys.argv)
